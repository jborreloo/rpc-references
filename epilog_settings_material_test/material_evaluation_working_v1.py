#note for any svg tuple (x,y) the corresponding coordinates are (w,h) or (width, height) on the artboard

import svgwrite

#initialize the artboard area, 30cmx30cm, and initialize the dimensional units, mm
dwg = svgwrite.Drawing('material_test_template.svg', size=('300mm','300mm'), viewBox=('0 0 300 300'))

tl = range(1,291,9)
#print tl
tr = range(9,300,9)
#print tr
#print len(tl), len(tr)
rects = range(3,300,9)

def boxdraw(tl,tr,bl,br,r,g,b):
	dwg.add(dwg.line(start=(tl), end=(tr), stroke=svgwrite.rgb(r,g,b)))
	dwg.add(dwg.line(start=(tl), end=(bl), stroke=svgwrite.rgb(r,g,b)))
	dwg.add(dwg.line(start=(tr), end=(br), stroke=svgwrite.rgb(r,g,b)))
	dwg.add(dwg.line(start=(bl), end=(br), stroke=svgwrite.rgb(r,g,b)))

for i in range(0,len(tl)):
    for j in range(0,len(tr)):
        boxdraw((tl[j],tl[i]),(tr[j],tl[i]),(tl[j],tr[i]),(tr[j],tr[i]),0.5*(i+j*5),1*(i+j*5),1.2*(i+j*5))

def rectdraw(w,h,r,g,b):
    dwg.add(dwg.rect(insert=(w,h), size=(4,4), rx=None, ry=None, fill=svgwrite.rgb(r,g,b)))

for a in range(0,len(rects)):
    for b in range(0,len(rects)):
        rectdraw(rects[b],rects[a],.5*(a+b*5),1*(a+b*5),1.2*(a+b*5))

dwg.save()
