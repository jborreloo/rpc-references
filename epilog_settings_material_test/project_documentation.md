#Epilog Material Calibration
##Project Documentation

**Intent:**
The goal of this document is to keep track of the design, development and deployment of a comprehensive test to identify the best speed, power and frequency settings for the Epilog Helix 60W laser on any stock material.

**Overview:**
While the Epilog user manual provides a chart with suggested laser settings best suited for a wide array of materials, there are situations in which a manufacturing outcome different from that described in the manual is desired, in addition to a whole host of materials that can be laser cut and etched, but are not described in the manual. By leveraging the color mapping abilities of the Epilog print driver, a given speed-power-frequency (SPF) combination can be mapped to a corresponding combination of red-green-blue (RGB) values. Thus, all SPF combinations desired to be tested can be mapped to a given RGB color and a testing array can be produced in Adobe Illustrator that could then be deployed on any laserable material that we would like to know the optimal settings for.

In order to produce this array, the goal is to leverage a [python] script to produce the map of all RGB and SPF combinations and then produce the image of the array (likely as an SVG file). Additionally, it would be ideal to use this same script to produce the configuration file that would specify in the epilog print client what each RGB color corresponds to in SPF settings - otherwise upwards of 400 unique values will need to be entered in manually.

**Reference Images**

![color mapping](https://bytebucket.org/jborreloo/rpc-references/raw/1243e5e43ef6dacbb0de51ccfdd8acc3e4ba3c52/epilog_material_calibration/color_mapping.PNG "the color mapping interface in the print client, demonstrating SPF 50-50-2500 mapped to RGB 250-100-150")

![advanced settings](https://bytebucket.org/jborreloo/rpc-references/raw/3c6ea4539dfd6abb5cd87cacc27d5d29d3c8f680/epilog_material_calibration/advanced_settings.PNG "the advanced settings tab of the print client; here, customized configurations can be uploaded, ideally pre-mapping colors to SPF values")

**Project Documentation**

* speed and power for cutting versus etching are mutually exclusive; however, speed and power themselves are not, i.e. - 
   * S-P: 50-25 != 25-50 != 50-50 != 25-25
   * so for any single speed setting, we must test all possible power combinations
   * and for any single power setting, we must test all possible speed combinations
* this becomes even more complicated when we incorporate frequency for cutting
   * frequency is not involved in etching, though
* thus, we should be able to use the same "testing unit" for both cutting and etching
   * the current plan is some kind of square tile, surrounded by a vector cut and with an interior etched square

* because there are so many possible combinations advancing each setting by integrers and because we want to have each testing unit take up enough space to be capable of visual inspection, we have to decide on an appropriate "step size" for each parameter that gives us enough data point resolution to make sound extrapolations, but also a sparse enough number to actually be manufacturingly feasible.

* so what will the testing battery look like?
   * set the ranges for speed, power, and frequency *[speed(0-a), power(0-b), frequency(0-c)]*
> speed(0)-power(0)-frequency(0)
> speed(0)-power(0)-<iterate on frequency for power(0)>
> speed(0)-power(0)-frequency(c)

> speed(0)-power(1)-frequency(0)
> ...
> speed(0)-power(1)-frequency(c)

> ...

> speed(0)-power(b)-frequency(c)

* so what is the math behind these combinations?
   * c combinations for s0-p0
   * c combinations for s0-p1
   * this is ultimately b numbers of c combinations for s0
   * and then there will be a numbers of combinations for b numbers of combinations for c
   * so it is indeed a * b * c

   * working with 20 values for each SPF component, though, gives 8000 testing units (20 * 20 * 20), which is far too many to fit onto a 12"x12" piece of material, whcih is what I was planning to design this test file for.

   * so now, the key is to figure out how best to array the three parameters. Once that's done the computational portion - at least for generating the image - shouldn't be too difficult (although the config file might be a bit tricky afterwards as well)
