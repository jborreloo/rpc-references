#Python Reference

###Tips, tricks, and reminders for python libraries I've used in the past

##RPi GPIO
**Uses Raspberry Pi GPIO pins like arduino digital pins**

```python
#import python library to control Pi GPIO pins
import RPi.GPIO as GPIO
#set up board interface
GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)
#initialize in/out pins
GPIO.setup(5,GPIO.OUT) #sets up RPi GPIO pin 5 as output
#GPIO.setup(5,GPIO.IN)

#turn pin on and off
GPIO.output(5,1)
GPIO.output(5,0)
```

##Glob and Serial
**establishing a serial connection (usually to arduino) with Raspberry Pi**

```python
import serial #serial communication library
import glob #directory-searching library

dev = glob.glob('/dev/tty*') #this only works when a single serial device is connected, otherwise glob will find multiple devices
if 'dev/ttyACM1' in dev:
	ser = serial.Serial('/dev/ttyACM1',115200)
else:
	ser = serial.Serial('/dev/ttyACM0',115200)

r = ser.readline() #equivalent to getting info from arduino serial monitor
```

##MySQLdb
**use [this link](http://zetcode.com/db/mysqlpython/) to find the tutorial I've always referenced for this. It's also saved in the software/python folder in bookmarks**

##Nonlinear Regression
**this makes use of the scipy.optimize library and can be used to determine the equation of best fit for a set of data following an initial guess input**

Guide found [here](http://www.walkingrandomly.com/images/python/least_squares/Python_nonlinear_least_squares.pdf)
