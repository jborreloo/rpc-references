##ProJet 3500 Post-Processing Protocol

*this protocol can be considered best practices as of 18 March 2016*

1. Preliminary wax melting in **oven** at **155F** (70C)

2. Clean **mineral oil** pre-heated in sonic cleaner to **55C** (130F)
> Try to avoid letting mineral oil temperature exceed (60C / 140F)

3. **Sonicate** parts for **30-45 minutes**

4. Pre-heat **soap-water** solution to about **70C** (155F)
> Once water reaches temperature, heat can be removed

> Amount of soap added varies with detergent
> > For micro-90 detergent, between 0.5 and 1.0 vol%

> > For dishsoap, between 1 and 5 vol%

* Briefly agitate water with parts added. Let sit for approximately 1 hour
