##Projet 3500 HDMax
* Getting access to remote diagnostic program
> to get access to the remote diagnostic program, you must remotely connect to the 3500, using a software such as windows remote desktop
> when you connect, you will see the login screen for the user ProJet_RH, the password into this account is projet_rh
> once logged in, you will be able to use the remote diagnostic software to see information on the entire printer system as well as continue to operate the printer via the GUI typically seen on the printer screen

* Modeling and positioning considerations
> Support material
> > The 3500 will always lay down a "raft" of wax support material prior to printing the model(s). This is done to allow the model(s) to be removed from the build tray after placement in the freezer.
>
> > This raft will cover the entire projection (or "shadow") of the model down to the build tray and is roughly half a centimeter thick.
>
> > > In cases where a model has an open space the raft layer will remain continuous beneath the model up to a certain sized opening (size TBD). As an example, a plate with many small perforations (a "swiss-cheese" structure) will be printed on top of a layer of wax support that does not include the perforations, due to their small size. A larger, ring-shaped structure, however, will not have support material in the central hole, but rather it will cover the regions directly beneath the ring in addition to a small area beyond the ring.
>

*reference images of wax support "raft" to be added subsequently*

> Small cavities
> > Some models may be made with very small surface cavities in the form of holes, channels or tubes. Specifically, these features would involve an open cut into the model surface without passing completely through the model. As a result, the use of support material on these features could be partially or completely avoided by orienting these cavities open-side-up.
>
> > In some cases, however, it might be preferential to print these features with support material filling in the cavities. This could be in order to keep spacing between two very close portions of model material, which could otherwise run together during the printing process, or to prevent some post-printing shrinkage.
>
> > To incorporate wax support ino these small cavities, simply orient the model upside-down, or in such way that the cavities open downward and there is model overhang in those regions. Then support material will be assured to fill in the cavities and can still be easily removed in post-processing.
>

*reference images of small cavities to be added subsequently*

> Captured geometry
> > "Captured geometry" refers to models that have a hollow portion within them but do not have any means of accessing that hollow region from the outside; take, for example, a hollow sphere designed in a CAD program.
>
> > Such captured geometry can be leveraged on the 3500 to print fine, complex structures - that otherwise might not be printable or handled without breaking - out of the wax support material and encased in the translucent model material (VisiJet Crystal)
>
> > Simply printing the hollow sphere as intended will not yield a ball of support material encased within the model material. Rather, the resulting model would be a solid sphere of model material with no support material within.
> > > This is due to a quirk in the slicing software with respect to its treatment of model surfaces. Without delving too deep into topological theory that I do not even know, a printed model, by the software's definition, must be made up of a single enclosed surface with all normals pointing away from that surface. In the case of our hollow sphere, there are two surfaces, which do not interact with one another - the inner surface and the outer surface. These surfaces come with their own sets of normals and, most importantly, are in no way topologically associated.
> >
> > > What the 3500 slicing software does, essentially, is ignore that inner surface and only look at the model with respect to its outer surface, since those normals point in the direction it expects model normals to point. This results in an overlooking of the interior and the sphere is printed now as hollow but as solid.
>
> > In order to print with captured geometry, the inner and outer surfaces must be merged with a cut. In order to prevent wax from exiting this cut, its dimensions should be exceedingly small.
>
> > The result is that the model topology now satifies the conditions for the deposition of support material in its ineterior but the opening that creates those conditions is too small to be resolved by the printer and is not created on the model.
>
> > **Important Considerations when printing with captured geometry**
> > > A cut on the order of a few nanometers is more than enough to satisfy the resolution requirements of this technique and typically also satisfies the slicer's topological requirements. Depending on how your STL file is meshed in the CAD software you are using, you may need to step the cut size up to a few microns. Anything 10um and lower should both meet topological needs and also be too small for printer resolution.
> >
> > > If you do want to be able to evacuate wax from a cavity, there must be a sufficiently large opening for easy evacuation. Typically an opening with a diameter of at least 3mm will suffice.
